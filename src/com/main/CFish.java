package com.main;

import com.inter.ISwimable;

public class CFish extends CAnimals implements  ISwimable  {
    private String gender = "";
    private int size = 0;
    private int age = 0;
    private boolean canEat = false;

    public CFish(String gender ,int size,int age,boolean canEat){
        this.gender = gender;
        this.size = size;
        this.age = age;
        this.canEat = canEat;

    }
    public void swim(){
        System.out.println("con ca cung co the boi");
    }
    @Override
    public String toString(){
        return "CFish {\"gender\":" + this.gender + ", age=" + this.age + ", canEat=" 
		+ this.canEat + ", size=" + this.size +"}";
    }

}
