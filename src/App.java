import com.main.CAnimals;
import com.main.CDuck;
import com.main.CFish;
import com.main.CZebra;

public class App {
    public static void main(String[] args) throws Exception {
        CAnimals donalDuck = new CDuck("male","white" , 100);
        System.out.println(donalDuck.toString());
        ((CAnimals)donalDuck).isMammal();
        CFish goldFish = new CFish("female", 10, 10, true);
        goldFish.swim();
        ((CAnimals)goldFish).mate();;
        System.out.println(goldFish.toString());
        CZebra xichthoZebra = new CZebra(200, "male", false);
        xichthoZebra.run();
        System.out.println(xichthoZebra);
        
    }
}
