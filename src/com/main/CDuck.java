package com.main;

import com.inter.IQuack;
import com.inter.ISwimable;

public class CDuck extends  CAnimals implements ISwimable,IQuack{
    private String gender = "";
    private String beakColor = "White";
    private int age = 0;

    public CDuck(String gender ,String beakColor,int age){
        this.gender = gender;
        this.beakColor = beakColor;
        this.age = age;

    }
   
    public void swim(){
        System.out.println("con vit co the boi");
    }
    public void quack(){
        System.out.println("con zvt keu  quack quack");
    }
    @Override
    public String toString(){
        return "CDuck {\"gender\":" + this.gender + ", age=" + this.age + ", color=" 
		+ this.beakColor + "}";
    }
}
