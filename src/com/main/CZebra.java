package com.main;

import com.inter.IRunable;

public class CZebra extends CAnimals implements IRunable {
    private int age = 0;
    private String gender = "";
    private boolean is_wild = false;

    public CZebra(int age,String gender,boolean is_wild){
        this.age = age;
        this.gender = gender;
        this.is_wild = is_wild;
    }
    public void run(){
        System.out.println("ngua co the chay");
    }
    @Override
    public String toString(){
        return "CZebra {\"gender\":" + this.gender + ", age=" + this.age + ", is_wild=" 
		+ this.is_wild + "}";
    }
}
